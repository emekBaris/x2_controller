%% Symbolic Jacobian and position vector caluclation of one leg X2
clc
clear
% Symbolic variables. See notebook for the explanations
syms xb yb thb th1 th2 h1 h2 l1 l2 l3 l4 l5 real

% Rotation Matrices
C_IB = [cos(thb) -sin(thb) 0;
        sin(thb) cos(thb)  0;
           0         0     1 ];    
I_r_IB = [xb; yb; 0; 0];
T_IB = [C_IB I_r_IB(1:3); [0 0 0 1]];


C_BQ1 = [cos(th1) -sin(th1) 0;
          sin(th1) cos(th1)  0;
           0         0     1 ];
B_r_BQ1 = [h2; -h1; 0; 0];
T_BQ1 = [C_BQ1 B_r_BQ1(1:3); [0 0 0 1]];


C_Q1M1 = eye(3);
Q1_r_Q1M1 = [0; -l2; 0; 0];
T_Q1M1 = [C_Q1M1 Q1_r_Q1M1(1:3); [0 0 0 1]];


C_M1Q2 = [cos(-th2) -sin(-th2) 0;
          sin(-th2) cos(-th2)  0;
           0         0     1 ];
M1_r_M1Q2 = [0; -(l3-l2); 0; 0];
T_M1Q2 = [C_M1Q2 M1_r_M1Q2(1:3); [0 0 0 1]];

C_Q2M2 = eye(3);
Q2_r_Q2M2 = [0; -l5; 0; 0];
T_Q2M2 = [C_Q2M2 Q2_r_Q2M2(1:3); [0 0 0 1]];


I_r_IM1 = I_r_IB + T_IB*B_r_BQ1 + T_IB*T_BQ1*Q1_r_Q1M1
I_r_IM2 = I_r_IM1 + T_IB*T_BQ1*T_Q1M1*M1_r_M1Q2 + T_IB*T_BQ1*T_Q1M1*T_M1Q2*Q2_r_Q2M2