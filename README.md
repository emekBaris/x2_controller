# README #

## Setup

Create a new virtual environment:

`conda create -n X2Control python`

`conda activate X2Control`

Install the required packages:

`pip install -r requirements.txt`

If you use additional packages, update requirements.txt:

`pip freeze > requirements.txt`

TODO: how to set up pycharm