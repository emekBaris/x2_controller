import canopen
import time
import numpy as np
import matplotlib.pyplot as plt
import os
import time

_controlWord = 0x6040
_statusWord = 0x6041
_modeOfOperation = 0x6060

_targetPosition = 0x607a
_targetVelocity = 0x60FF
_targetTorque = 0x6071
_profileTargetVelocity = 0x6081
_profileAcceleration = 0x6083
_profileDeceleration = 0x6084
_actualPosition = 0x6063


def address2info(node, address):
    sdo_info = node.sdo[address]
    print("Operation Name:", sdo_info.name)
    print("value: 0x%X" % sdo_info.raw)
    return sdo_info

def degree2cnt(deg):
    count = deg*10000/5.14
    return count

def rpm2dec(rpm):
    # unit of speed is rpm and internal unit is dec
    # max velocity 4000ish
    encoder_res = 10000
    dec = rpm*512*encoder_res/1875
    return int(dec)

# Start with creating a network representing one CAN bus
network = canopen.Network()

# Connect to the CAN bus
# (see https://python-can.readthedocs.io/en/latest/bus.html).
network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=1000000)

# This will attempt to read an SDO from nodes 1 - 127
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.1)
for node_id in network.scanner.nodes:
    print("Found node %d!" % node_id)

# node = network.add_node(1, 'CopleyAmp_yw.eds')
node = network.add_node(4, 'eds/CopleyAmp.eds')

node.nmt.state = 'RESET'
time.sleep(1)
node.nmt.state = 'OPERATIONAL'

# control_word_node = node.sdo[_controlWord]
# control_word_node.raw = 0x06
# # reset fault
# control_word_node = node.sdo[_controlWord]
# control_word_node.raw = 0x80
# # enable operation
# control_word_node = node.sdo[_controlWord]
# control_word_node.raw = 0x06

address2info(node, _modeOfOperation)
address2info(node, _controlWord)
address2info(node, _statusWord)
node.sdo[_controlWord].raw = 0x86
node.sdo[_modeOfOperation].raw = 0x04
node.sdo[_targetPosition].raw = 0
node.sdo[_profileTargetVelocity].raw = 0
node.sdo[_profileAcceleration].raw = 500000
node.sdo[_profileDeceleration].raw = 500000
node.sdo[_targetVelocity].raw = 0
time.sleep(0.1)
# node.sdo[_controlWord].raw = 0x103f
# node.sdo[_modeOfOperation].raw = 0x03
# print("------------")
# address2info(node, _modeOfOperation)
# address2info(node, _controlWord)
# address2info(node, _statusWord)

print("*****************************************")
mode = 2 # 1 velocity, 2 torque

if(mode == 1):
    node.sdo[_modeOfOperation].raw = 3
elif(mode == 2):
    node.sdo[_modeOfOperation].raw = 4
initialTime = time.time()
while(time.time() - initialTime < 1.0):
    node.sdo[_controlWord].raw = 0x0f
    if (mode == 1):
        node.sdo[_targetVelocity].raw = rpm2dec(300)
    elif (mode == 2):
        node.sdo[_targetTorque].raw = 300
    address2info(node, _statusWord)

node.nmt.state = 'RESET'
