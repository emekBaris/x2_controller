from src.x2_example_controller import ExampleController
import time

def main():
    """ The main() function. """
    controller = ExampleController()

    print("***************")
    initialTime = time.time()
    while (time.time() - initialTime < 1.0):
        controller.advance()

    controller.disconnect()
    controller.postProcess()

if __name__ == '__main__':
        main()
