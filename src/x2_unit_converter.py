import math

def count2rad(count):
    '''
    converting encoder count to rad
    :param count: encoder value, default message from can
    :return: rad
    '''
    return count / 159715.1684916456

def rad2count(rad):
    '''
    converting rad to encoder count
    :param rad: joint position in rad
    :return: encoder value, default message from can
    '''
    return rad * 159715.1684916456

def count2deg(count):
    '''
    converts encoder count to deg
    :param count: encoder value, default message from can
    :return: deg
    '''
    return count / 2787.555555539698

def deg2count(count):
    '''
    converts deg to encoder count
    :param deg: joint position in deg
    :return: encoder value, default message from can
    '''
    return count * 2787.555555539698


def countPerTenthSec2radPerSec(countPerTenthSec):
    '''
    coverts count/0.1sec to rad/sec
    :param countPerTenthSec: default velocity message from can encoder count per 0.1 sec
    :return: rad/sec
    '''
    return countPerTenthSec / 1597151.684916456

def radPerSec2countPerTenthSec(radPerSec):
    '''
    coverts rad/sec to count/0.1sec
    :param rad/sec: joint velocity in rad/sec
    :return: countPerTenthSec: default velocity message from can encoder count per 0.1 sec
    '''
    return radPerSec * 1597151.684916456

def countPerTenthSec2degPerSec(countPerTenthSec):
    '''
    coverts count/0.1sec to deg/sec
    :param countPerTenthSec: default velocity message from can encoder count per 0.1 sec
    :return: deg/sec
    '''
    return countPerTenthSec / 27875.55555539698

def degPerSec2countPerTenthSec(degPerSec):
    '''
    coverts deg/sec to count/0.1sec
    :param deg/sec: joint velocity in deg/sec
    :return: countPerTenthSec: default velocity message from can encoder count per 0.1 sec
    '''
    return degPerSec * 27875.55555539698

def ratedTorque2torque(ratedTorque):
    '''
    converts rated torque in Nmm to Torque in Nm
    :param ratedTorque: default unit for can communication [0.319 N.m]
    :return: torque [Nm]
    '''
    return ratedTorque * 0.0390775 # Gear_ratio(122.5)*rated_torque(319Nmm)/1000

def torque2ratedTorque(torque):
    '''
    converts rated torque in Nmm to Torque in Nm
    :param torque [Nm]
    :return: ratedTorque: default unit for can communication [0.319 N.m]
    '''
    return torque / 0.0390775

def deg2rad(deg): # converting encoder count to deg
    return deg*math.pi/180.0

def rad2deg(rad): # converting encoder count to deg
    return rad*180.0/math.pi