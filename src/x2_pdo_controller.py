import canopen
import time
import numpy as np
import matplotlib.pyplot as plt
import os

target_torque = 0x6071
target_position = 0x607A
target_velocity = 0x60ff
profile_velocity = 0x6081
profile_acceleration = 0x6083
profile_deceleration = 0x6084

curr_loop_gains = 0x60F6

profile_position = 0x6086  # not right

actual_position = 0x6063
actual_velocity = 0x606c
actual_torque = 0x6077
actual_current = 0x6078
error_code = 0x603f     # not right
status_word = 0x6041
control_word = 0x6040
operation_mode = 0x6060
# error_code = 0x2601

velocity_offset = 0x60B1
torque_offset = 0x60B2

_controlWord = 0x6040
_statusWord = 0x6041
_modeOfOperation = 0x6060

_targetPosition = 0x607a
_targetVelocity = 0x60FF
_targetTorque = 0x6071
_profileTargetVelocity = 0x6081
_profileAcceleration = 0x6083
_profileDeceleration = 0x6084
_actualPosition = 0x6063

def init_network():
    # Start with creating a network representing one CAN bus
    network = canopen.Network()

    # Connect to the CAN bus
    # (see https://python-can.readthedocs.io/en/latest/bus.html).
    network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=1000000)

    # This will attempt to read an SDO from nodes 1 - 127
    network.scanner.search()
    # We may need to wait a short while here to allow all nodes to respond
    time.sleep(0.1)
    # for node_id in network.scanner.nodes:
    #     print("Found node %d!" % node_id)

    node = network.add_node(4, 'eds/CopleyAmp.eds')
    # node = network.add_node(4)
    # # reset error
    # resetError(node)

    return network, node

def rpss2dec(rpss):
    # unit of speed is rpm and internal unit is dec
    # max acceleration 1500ish
    encoder_res = 10000
    dec = rpss*65536*encoder_res/4000000
    return int(dec)

def rpm2dec(rpm):
    # unit of speed is rpm and internal unit is dec
    # max velocity 4000ish
    encoder_res = 10000
    dec = rpm*512*encoder_res/1875
    return int(dec)

def dec2rpm(dec):
    encoder_res = 10000
    rpm = dec*1875/(512*encoder_res)
    return rpm

def addree2name(node, address):
    sdo_info = node.sdo[address]
    print("Operation Name:", sdo_info.name)
    print("value: 0x%X" % sdo_info.raw)
    print("{0:b}".format(sdo_info.raw))
    return sdo_info

def print_current(message):
    print("%s received" % message.name)
    for var in message:
        print("%s = 0x%x" % (var.name, var.raw))

def resetError(node):
    # shutdown
    control_word_node = node.sdo[control_word]
    control_word_node.raw = 0x06
    # reset fault
    control_word_node = node.sdo[control_word]
    control_word_node.raw = 0x80
    # enable operation
    control_word_node = node.sdo[control_word]
    control_word_node.raw = 0x06

def degree2cnt(deg):
    count = deg*10000/5.14
    return count

def count2deg(count):
    deg = count*5.14/10000
    return deg

def tracking_measurements(node, measurement):
    for k in range(1, 3000):
        addree2name(node, measurement)

def initializatin(node):
    print("Initialization ... ")
    # set control word
    control_word_node = node.sdo[control_word]
    # power up
    control_word_node.raw = 0x0f

    # # set control model to position control
    # operation_mode_node = node.sdo[operation_mode]
    # operation_mode_node.raw = 0x01

    # # set control model to cyclic synchronous position mode
    # operation_mode_node = node.sdo[operation_mode]
    # operation_mode_node.raw = 0x07

    # set control model to cyclic synchronous position mode
    operation_mode_node = node.sdo[operation_mode]
    operation_mode_node.raw = 0x03

    addree2name(node, operation_mode)

def rpdo_conf(node):
    # addree2name(node, status_word)
    # addree2name(node, actual_torque)
    # addree2name(node, actual_position)
    #
    # addree2name(node, target_velocity)
    # addree2name(node, control_word)
    # addree2name(node, operation_mode)

    # Re-map RPDO[1]
    # sent data to device

    node.rpdo.read()
    node.rpdo[2].clear()
    node.rpdo[2].add_variable(control_word, length=16)
    node.rpdo[2].add_variable(operation_mode, length=8)
    node.rpdo[2].trans_type = 254 # 254
    # node.rpdo[4].inhibit_time = 2
    node.rpdo[2].enabled = True
    node.rpdo[2].save()

    node.rpdo.read()
    node.rpdo[3].clear()
    node.rpdo[3].add_variable(_targetVelocity, length = 32) # for velocity control
    node.rpdo[3].trans_type = 254 # 254
    node.rpdo[3].enabled = True
    node.rpdo[3].save()

    node.rpdo.read()
    node.rpdo[4].clear()
    node.rpdo[4].add_variable(_targetTorque, length = 32)
    node.rpdo[4].trans_type = 254 # 254
    node.rpdo[4].enabled = True
    node.rpdo[4].save()

def tpdo_conf(node):
    # Re-map TPDO[1]
    # receive data from device
    node.tpdo.read()
    index = 1
    node.tpdo[index].clear()
    node.tpdo[index].add_variable(actual_position)
    # node.tpdo[index].add_variable(status_word)
    node.tpdo[index].trans_type = 1 # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
    # node.tpdo[index].inhibit_time = 0
    node.tpdo[index].enabled = True

    # Re-map TPDO[2]
    index = 2
    node.tpdo[index].clear()
    node.tpdo[index].add_variable(actual_velocity)
    # node.tpdo[index].add_variable(actual_torque)
    node.tpdo[index].trans_type = 1 # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
    node.tpdo[index].enabled = True

    # Re-map TPDO[3]
    index = 3
    node.tpdo[index].clear()
    node.tpdo[index].add_variable(actual_torque)
    node.tpdo[index].trans_type = 1  # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
    node.tpdo[index].enabled = True
    node.tpdo.save()


def set_position_mode(node):
    ''' Setting profile velocity, acceleration, deceleration'''
    # set velocity
    velocity_node = node.sdo[profile_velocity]
    velocity_node.raw = rpm2dec(400)

    # set acceleration
    acc_node = node.sdo[profile_acceleration]
    acc_node.raw = rpss2dec(100) # acceleration too high will cause jerk

    # set deceleration
    dec_node = node.sdo[profile_deceleration]
    dec_node.raw = rpss2dec(100)

    # instant position mode; important
    control_word_node = node.sdo[control_word]
    control_word_node.raw = 0x103f

# def set_velocity_mode(node):
#     ''' Setting profile velocity, acceleration, deceleration'''
#     # set velocity
#     velocity_node = node.sdo[profile_velocity]
#     velocity_node.raw = rpm2dec(400)
#
#     # set acceleration
#     acc_node = node.sdo[profile_acceleration]
#     acc_node.raw = rpss2dec(100)  # acceleration too high will cause jerk
#
#     # set deceleration
#     dec_node = node.sdo[profile_deceleration]
#     dec_node.raw = rpss2dec(100)
#
#     # instant position mode; important
#     control_word_node = node.sdo[control_word]
#     control_word_node.raw = 0x103f

def set_recording(node):
    # shutdown
    control_word_node = node.sdo[control_word]
    control_word_node.raw = 0x06

actualVelocityRecord = []
timeRecord = []
dtRecord = []
def velocityCallback(message):
    time1 = time.time()
    actualVelocityRecord.append(dec2rpm(message[0].raw))
    dtRecord.append((time.time() - time1)*1000)
    timeRecord.append(time.time() - initialTime)

# def velocityCallback(can_id, message, timestamp):
#     # print("IN CALLBACK")
#     actualVelocityRecord.append(message)
#     # timeRecord.append(time.time() - initialTime)

[network, node] = init_network()

'''Initialization'''
# initializatin(node)

''' Setup tPDO, rPDO'''
# Save new configuration (node must be in pre-operational)
node.nmt.state = 'RESET'
time.sleep(1)
node.nmt.state = 'PRE-OPERATIONAL'

# PDO settings
rpdo_conf(node)
tpdo_conf(node)

# set_position_mode(node)

''' Start sync'''
# Transmit SYNC every 0.01 s
# the actual cycle is about 100hz with 0.009
network.sync.start(0.001)
# Change state to operational (NMT start)

node.nmt.state = 'OPERATIONAL'

# addree2name(node, _modeOfOperation)
# addree2name(node, _controlWord)
# addree2name(node, _statusWord)
node.sdo[_controlWord].raw = 0x0f
node.sdo[_modeOfOperation].raw = 0x03
node.sdo[_targetPosition].raw = 0
node.sdo[_profileTargetVelocity].raw = 0
node.sdo[_profileAcceleration].raw = 500000
node.sdo[_profileDeceleration].raw = 500000
node.sdo[_targetVelocity].raw = 0
# time.sleep(0.1)

print("*****************************************")
node.sdo[_modeOfOperation].raw = 3
initialTime = time.time()
prevTime = time.time()
dtRecord = []

node.tpdo[2].add_callback(velocityCallback)
# network.subscribe(node.tpdo[2].cob_id, velocityCallback)
# print("!!!!!!!!!!!!!!!!!!!!!!!!!!")
# print(node.tpdo[2].cob_id)

# print(node.sdo_channels)
node.rpdo[2].start(0.001)
node.rpdo[3].start(0.001)
# node.tpdo[2].start(0.001)

node.rpdo[2][_modeOfOperation].raw = 3
while(time.time() - initialTime < 2.0):

    # node.tpdo[2].wait_for_reception()
    # actualVelocityRecord.append(dec2rpm(node.tpdo[2][actual_velocity].raw))
    #
    node.rpdo[2][_controlWord].raw = 0x0f
    node.rpdo[3][_targetVelocity].raw = rpm2dec(200*(time.time() - initialTime)) # for velocity control
    # node.rpdo[2].transmit()
    # node.rpdo[3].transmit()

print("loop end")
node.nmt.state = 'RESET'

# Stop transmission of RxPDO
# node.rpdo[3].stop()
network.sync.stop()
network.disconnect()
#
plt.figure()
plt.plot(actualVelocityRecord)
plt.xlabel('# of data')
plt.ylabel("joint speed")
plt.grid(True)
plt.show()

print(dtRecord)
plt.figure()
plt.plot(dtRecord)
plt.xlabel('# of data')
plt.ylabel("time spent in callback")
plt.grid(True)
plt.show()

print(timeRecord)
plt.figure()
plt.plot(timeRecord)
plt.xlabel('# of data')
plt.ylabel("total time")
plt.grid(True)
plt.show()

# plt.figure()
# plt.plot(dtRecord)
# plt.show()
#
print(len(actualVelocityRecord))
values, counts = np.unique(actualVelocityRecord, return_counts=True)
print("unique: ", values.shape)
print("The end")



