import canopen
import time
from enum import Enum
import src.x2_unit_converter as convert
import numpy as np

class Joint(object):
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.node = None
        self.modeOfOperation = None
        self.desiredVel = 0.0 # [rad/s]
        self.desiredTorque = 0.0 # [N.m]
        self.actualPos = 0.0 # [rad]
        self.actualVel = None # [rad/s]
        self.actualTorque = None # [N/m]

class ModeOfOperation(Enum):
    VELOCITY = 3
    TORQUE = 4

class X2Communication(object):
    """ Class for PDO Communication
    """
    def __init__(self, dt): # todo: add which joints to control as argument
        # declaring addresses
        self.controlWordAddress = 0x6040
        self.statusWordAddress = 0x6041
        self.modeOfOperationAddress = 0x6060
        self.targetPositionAddress = 0x607a
        self.targetVelocityAddress = 0x60FF
        self.targetTorqueAddress = 0x6071
        self.profileTargetVelocityAddress = 0x6081
        self.profileAccelerationAddress = 0x6083
        self.profileDecelerationAddress = 0x6084
        self.actualPositionAddress = 0x6063
        self.actualVelocityAddress = 0x606c
        self.actualTorqueAddress = 0x6077
        self.actualCurrentAddress = 0x6078
        self.currentLoopGainsAddress = 0x60F6
        self.homingMethodAddress = 0X6098
        self.homingSpeedsAddress = 0X6099

        # Creating a network representing one CAN bus
        self.network = canopen.Network()

        # Connect to the CAN bus
        # (see https://python-can.readthedocs.io/en/latest/bus.html).
        self.network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=1000000)

        self.printAvailableNodes()

        id = 0
        leftHipJoint =   Joint("left hip", id)
        id += 1
        leftKneeJoint =  Joint("left knee", id)
        id += 1
        rightHipJoint =  Joint("right hip", id)
        id += 1
        rightKneeJoint = Joint("right knee", id)

        self.joints = [leftHipJoint, leftKneeJoint, rightHipJoint, rightKneeJoint]

        for joint in self.joints:
            if joint.name == "left hip":
                node_id = 1
            elif joint.name == "left knee":
                node_id = 2
            elif joint.name == "right hip":
                node_id = 3
            elif joint.name == "right knee":
                node_id = 4
            joint.node = self.network.add_node(node_id, 'eds/CopleyAmp.eds') # +1 because joint id starts from 1, np array starts from 0

        self.actuatorNodes = [] # initializing the list that keeps all the nodes.
        for joint in self.joints:
            self.actuatorNodes.append(joint.node)

        # resetting nodes and then setting pre-operational state
        for actuatorNode in self.actuatorNodes:
            actuatorNode.nmt.state = 'RESET'
            time.sleep(0.5)
            actuatorNode.nmt.state = 'PRE-OPERATIONAL'

        # calling rpdo and tpdo remapping functions
        self._rpdoRemap()
        self._tpdoRemap()

        # self.joints[3].node.tpdo[2].add_callback(self.velocityCallback) #todo: delete. this is only for testing

        # set sync freq
        self.network.sync.start(dt)

        # set state of actuator nodes to operational
        for actuatorNode in self.actuatorNodes:
            actuatorNode.nmt.state = 'OPERATIONAL'

        self.it = 0 #todo: delete

    def velocityCallback(self, message): # not being used. For test purposes
        self.it += 1
        print(self.it)
        self.joints[3].actualPos = message[0].raw

    def printAvailableNodes(self):
        self.network.scanner.search()
        # We may need to wait a short while here to allow all nodes to respond
        time.sleep(0.1)
        for node_id in self.network.scanner.nodes:
            print("Found node %d!" % node_id)

    def _rpdoRemap(self):
        # remapping RPDO to send commands to actuators
        for actuatorNode in self.actuatorNodes:
            # RPDO2 -> control word and mode of operation
            actuatorNode.rpdo.read()
            actuatorNode.rpdo[2].clear()
            actuatorNode.rpdo[2].add_variable(self.controlWordAddress, length=16)
            actuatorNode.rpdo[2].add_variable(self.modeOfOperationAddress, length=8)
            actuatorNode.rpdo[2].trans_type = 254  # 254
            actuatorNode.rpdo[2].enabled = True
            actuatorNode.rpdo[2].save()

            # RPDO3 -> target velocity
            actuatorNode.rpdo.read()
            actuatorNode.rpdo[3].clear()
            actuatorNode.rpdo[3].add_variable(self.targetVelocityAddress, length=32)  # for velocity control
            actuatorNode.rpdo[3].trans_type = 254  # 254
            actuatorNode.rpdo[3].enabled = True
            actuatorNode.rpdo[3].save()

            # RPDO4 -> target torque
            actuatorNode.rpdo.read()
            actuatorNode.rpdo[4].clear()
            actuatorNode.rpdo[4].add_variable(self.targetTorqueAddress, length=32)
            actuatorNode.rpdo[4].trans_type = 254  # 254
            actuatorNode.rpdo[4].enabled = True
            actuatorNode.rpdo[4].save()

    def _tpdoRemap(self):
        # remapping TPDO to receive info from actuators
        for actuatorNode in self.actuatorNodes:
            # TPDO1 -> actual position
            actuatorNode.tpdo.read()
            actuatorNode.tpdo[1].clear()
            actuatorNode.tpdo[1].add_variable(self.actualPositionAddress)
            actuatorNode.tpdo[1].trans_type = 1  # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
            # actuatorNode.tpdo[1].event_timer = 10
            actuatorNode.tpdo[1].enabled = True
            actuatorNode.tpdo[1].save()

            # TPDO2 -> actual velocity
            actuatorNode.tpdo.read()
            actuatorNode.tpdo[2].clear()
            actuatorNode.tpdo[2].add_variable(self.actualVelocityAddress)
            actuatorNode.tpdo[2].trans_type = 1  # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
            # actuatorNode.tpdo[2].event_timer = 10
            actuatorNode.tpdo[2].enabled = True
            actuatorNode.tpdo[2].save()

            # TPDO2 -> actual torque
            actuatorNode.tpdo.read()
            actuatorNode.tpdo[3].clear()
            actuatorNode.tpdo[3].add_variable(self.actualTorqueAddress)
            actuatorNode.tpdo[3].trans_type = 1  # http://www.ars-informatica.com/Root/Code/CANOPEN/CommunicationParametersTPDO.aspx
            # actuatorNode.tpdo[3].event_timer = 10
            actuatorNode.tpdo[3].enabled = True
            actuatorNode.tpdo[3].save()

    def getJointPositions(self):
        '''
        getting joint positions
        :return: jointPositions <np.ndarray> size: (4)
        '''

        jointPositions = np.zeros(4)
        for joint in self.joints:
            jointPositions[joint.id] = joint.actualPos
        return jointPositions

    def getJointVelocities(self):
        '''
        getting joint velocities
        :return: jointVelocities <np.ndarray> size: (4)
        '''

        jointVelocities = np.zeros(4)
        for joint in self.joints:
            jointVelocities[joint.id] = joint.actualVel
        return jointVelocities

    def getJointTorques(self):
        '''
        getting joint torques
        :return: jointTorques <np.ndarray> size: (4)
        '''

        jointTorques = np.zeros(4)
        for joint in self.joints:
            jointTorques[joint.id] = joint.actualTorque
        return jointTorques

    def setModeOfOperation(self, modeOfOperation):
        '''
        :param modeOfOperation: <list of ModeOfOperation>
        :return: None
        '''
        for joint in self.joints:
            joint.modeOfOperation = modeOfOperation[joint.id]
            joint.node.sdo[self.modeOfOperationAddress].raw = joint.modeOfOperation.value

    def setDesiredJointVelocities(self, desiredJointVelocities):
        '''
        :param desiredJointVelocities: <numpy.ndarray>
        :return: None
        '''
        assert desiredJointVelocities.shape[0] == len(self.joints) # size of desired vel should be equal to size of joints

        for joint in self.joints: # assigning joint desired velocities
            joint.desiredVel = desiredJointVelocities[joint.id]

    def setDesiredJointTorques(self, desiredJointTorques):
        '''
        :param desiredJointTorques: <numpy.ndarray>
        :return: None
        '''
        assert desiredJointTorques.shape[0] == len(self.joints) # size of desired vel should be equal to size of joints

        for joint in self.joints: # assigning joint desired velocities
            joint.desiredTorque = desiredJointTorques[joint.id]

    # TODO: get joint pos vel and torque functions

    def setCurrentLoopGains(self, currentLoopGains):
        '''
        Setting the current loop gains
        :param currentLoopGains: <np.ndarray> size: 3x4. Row 0:kp, Row1:ki, Row2: offset. Columns: motors
        :return: None
        '''

        for joint in self.joints:
            for j in range(3):
                joint.node.sdo[self.currentLoopGainsAddress][j + 1].raw = currentLoopGains[j, joint.id]

    def getCurrentLoopGains(self):
        '''
        Getting the current loop gains
        :return: currentLoopGains: <np.ndarray> size: 3x4. Row 0:kp, Row1:ki, Row2: offset. Columns: motors
        '''

        currentLoopGains = np.zeros((3, 4))
        for joint in self.joints:
            for j in range(3):
                currentLoopGains[j, joint.id] = joint.node.sdo[self.currentLoopGainsAddress][j+1].raw
        return currentLoopGains

    def homing(self, maxTime, jointArr, fastSpeed = convert.deg2rad(5), slowSpeed = convert.deg2rad(1)):
        '''
        homing function. Has its own loop. keeps in here until homing is finished or maximum time is reached
        :param maxTime: maximum time allowed for homing operation [sec]
        :param jointArr: list of booleans indicating homing should be done or not for the corresponding joint
        :param fastSpeed: fast speed for the homing operation [rad/s]
        :param slowSpeed: slow speed for the homing operation [rad/s]
        :return: success list of booleans indicating success of homing for the corresponding joint
        '''

        t0 = time.time()
        for joint in self.joints:
            if jointArr[joint.id] == False: # skip if it is not asked to do homing for that joint
                continue
            joint.node.sdo[self.homingSpeedsAddress][1].raw = convert.radPerSec2countPerTenthSec(fastSpeed)
            joint.node.sdo[self.homingSpeedsAddress][2].raw = convert.radPerSec2countPerTenthSec(slowSpeed)
            joint.node.sdo[self.modeOfOperationAddress].raw = 6 # homing mode
            joint.node.sdo[self.homingMethodAddress].raw = -2  # homing method -2: hardstop, initial motion in negative direction
            joint.node.sdo[self.controlWordAddress].raw = 0x1f # start homing

        success = [False] * sum(jointArr)
        while((sum(success) != sum(jointArr)) and time.time() - t0 < maxTime):
            success = []
            for joint in self.joints:
                if jointArr[joint.id] == False:  # skip if it is not asked to do homing for that joint
                    continue
                success.append(joint.node.sdo[self.statusWordAddress].raw >> 12 & 1)  # 12th bit of status word gives if homing is attained

        for joint in self.joints: # after hıming attempt change the control mode
            if jointArr[joint.id] == False: # skip if it is not asked to do homing for that joint
                continue
            joint.node.sdo[self.controlWordAddress].raw = 0x0f

        return success

    def disconnect(self):
        for actuatorNode in self.actuatorNodes:
            actuatorNode.nmt.state = 'RESET'
            actuatorNode.rpdo[2].stop()
            actuatorNode.rpdo[3].stop()
            actuatorNode.rpdo[4].stop()
        self.network.sync.stop()
        self.network.disconnect()
        print("Disconnected!")

    def receive(self):
        # todo: this is not a good way. What is the best way to read values?
        #  When callbacks are used in synchronous mode(trans_type = 1), it gets in callback function
        #  much less, don't now the reason.
        #  If trans_type = 254-255, it doesn't get in. Also can't set event_timer. Gives subindex 5 exceeds error.
        self.joints[1].node.tpdo[1].wait_for_reception()
        for joint in self.joints:
            joint.actualPos = convert.count2rad(joint.node.tpdo[1][self.actualPositionAddress].raw)
            joint.actualVel = convert.countPerTenthSec2radPerSec(joint.node.tpdo[2][self.actualVelocityAddress].raw)
            joint.actualTorque = convert.ratedTorque2torque(joint.node.tpdo[3][self.actualTorqueAddress].raw)

    def send(self):
        for joint in self.joints:
            self.it += 1
            joint.node.rpdo[2][self.controlWordAddress].raw = 0x0f
            joint.node.rpdo[2].transmit()
            # joint.node.rpdo[2][self.modeOfOperationAddress].raw = joint.modeOfOperation.value
            if(joint.modeOfOperation == ModeOfOperation.VELOCITY):# if velocity control
                joint.node.rpdo[3][self.targetVelocityAddress].raw = convert.radPerSec2countPerTenthSec(joint.desiredVel)
                joint.node.rpdo[3].transmit()
            elif(joint.modeOfOperation == ModeOfOperation.TORQUE): # if torque control
                joint.node.rpdo[4][self.targetTorqueAddress].raw = convert.torque2ratedTorque(joint.desiredTorque)
                joint.node.rpdo[4].transmit()
            else:
                print("[", __name__,"]: Unhandled Mode Of Operation!")

        # TODO: Is it an efficient way of sending commands?

    def communicate(self):
        self.receive()
        self.send()
        # print(self.joints[3].node.sdo[0X6076].raw)










