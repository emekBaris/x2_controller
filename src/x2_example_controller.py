from src.x2_pdo_communication import X2Communication, ModeOfOperation
import src.x2_unit_converter as convert
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
import time

class ExampleController(object):
    """ Class for a simple example controller
    """
    def __init__(self):
        print("Example Controller is initialized!")
        dt = 0.001
        self.x2Com = X2Communication(dt = dt)

        self.recordTime = []
        self.actualJointTorqueRecord = []
        self.desiredJointTorqueRecord = []

        currentLoopGains = np.array([[100, 100, 100, 100],
                                     [115, 115, 115, 115],
                                     [0,     0,   0,   0]]) # 3 different gains(kp, ki, offset) for 4 different joints

        self.x2Com.setCurrentLoopGains(currentLoopGains)
        homingSuccess = self.x2Com.homing(maxTime= 120, jointArr=[False, False, False, True], slowSpeed=convert.deg2rad(10))
        print("Homing: ", homingSuccess)

        self.control_mode = 3

        # self.x2Com.setModeOfOperation([ModeOfOperation.VELOCITY, ModeOfOperation.VELOCITY,
        #                                ModeOfOperation.VELOCITY, ModeOfOperation.VELOCITY])

        if(self.control_mode == 3):
            self.x2Com.setModeOfOperation([ModeOfOperation.TORQUE, ModeOfOperation.TORQUE,
                                           ModeOfOperation.TORQUE, ModeOfOperation.TORQUE])

        self.t0 = time.time()

    def advance(self):

        if self.control_mode == 0:
            self.x2Com.receive()
            if(convert.rad2deg(self.x2Com.joints[3].actualPos) == 0):
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print("status: ", self.x2Com.joints[3].node.sdo[0x6041].raw >> 12 & 1)

        if self.control_mode == 1: # sin velocity
            desiredVel = np.zeros(4)

            t = time.time() - self.t0

            A = convert.deg2rad(80) # amplitude of the sine wave [rad/s]
            T = 4.0 # period of sine wave [s]

            A_hip = convert.deg2rad(40) # amplitude of the sine wave [rad/s]
            T_hip = 4.0 # period of sine wave [s]

            desiredVel[0] = A_hip*np.sin(2*np.pi/T_hip*t) # left hip
            desiredVel[1] = A*np.sin(2*np.pi/T*t) # left knee
            desiredVel[2] = A_hip*np.sin(2*np.pi/T_hip*t) # right hip
            desiredVel[3] = A*np.sin(2*np.pi/T*t) # right knee

            self.x2Com.setDesiredJointVelocities(desiredVel)

            self.x2Com.communicate()

            self.recordActualKnee.append(convert.rad2deg(self.x2Com.joints[3].actualVel))
            self.recordDesiredKnee.append(convert.rad2deg(desiredVel[3]))
            self.recordActualHip.append(convert.rad2deg(self.x2Com.joints[2].actualVel))
            self.recordDesiredHip.append(convert.rad2deg(desiredVel[2]))
            self.recordTime.append(t)

        elif self.control_mode == 2: # unilateral spring
            t = time.time() - self.t0

            desiredTorque = np.zeros(4)

            k_spring = 800 # virtual spring constant between knee joints [N.m/rad]

            desiredTorque[0] = 0 # left hip
            desiredTorque[1] = k_spring*(self.x2Com.joints[3].actualPos - self.x2Com.joints[1].actualPos)
            desiredTorque[2] = 0 # right hip
            desiredTorque[3] = 0 # [N.m]
            self.x2Com.setDesiredJointTorques(desiredTorque)
            self.x2Com.communicate()

            self.recordActualKnee.append(convert.rad2deg(self.x2Com.joints[1].actualPos))
            self.recordDesiredKnee.append(convert.rad2deg(self.x2Com.joints[3].actualPos))
            self.recordTime.append(t)

        elif self.control_mode == 3: #sin torque
            desiredTorque = [0]*4 # initializing the desired torque list

            t = time.time() - self.t0

            A = 12 # amplitude of the sine wave [rad/s]
            T = 4.0 # period of sine wave [s]

            desiredTorque[0] = 0*A*np.sin(2*np.pi/T*t) # left hip
            desiredTorque[1] = 0*A*np.sin(2*np.pi/T*t) # left knee
            desiredTorque[2] = 0*A*np.sin(2*np.pi/T*t) # right hip
            desiredTorque[3] = A*np.sin(2*np.pi/T*t)

            # print("value: ", self.x2Com.actuatorNodes[3].sdo[0x60f6][1].raw)

            # print(self.x2Com.actuatorNodes[3].sdo.upload(0x2380,0))

            self.x2Com.setDesiredJointTorques(np.array(desiredTorque))
            self.x2Com.communicate()

            self.actualJointTorqueRecord.append([self.x2Com.joints[0].actualTorque,
                                                 self.x2Com.joints[1].actualTorque,
                                                 self.x2Com.joints[2].actualTorque,
                                                 self.x2Com.joints[3].actualTorque])

            self.desiredJointTorqueRecord.append(desiredTorque)

            self.recordTime.append(t)

    def disconnect(self):
        self.currentLoopGains = self.x2Com.getCurrentLoopGains()
        self.x2Com.disconnect()

    def postProcess(self):

        plot_flag = [True, True, True, True] # Flag of plotting which joints
        for i in range(len(plot_flag)):
            if(not plot_flag[i]):
                continue
            fig, ax = plt.subplots()
            ax.plot(self.recordTime, [item[i] for item in self.actualJointTorqueRecord], 'b')
            ax.plot(self.recordTime, [item[i] for item in self.desiredJointTorqueRecord], 'r')

            ax.legend(['actual', 'desired'])
            plt.xlabel('time[s]')
            plt.ylabel('joint torque [N.m]')
            title = "Joint: " + str(i) + " P: " + str(self.currentLoopGains[0,i]) +\
                    " I: " + str(self.currentLoopGains[1, i])
            plt.title(title)

            ax.set_ylim(0, 14)

            # Change major ticks to show every 20.
            # ax.xaxis.set_major_locator(MultipleLocator(1))
            ax.yaxis.set_major_locator(MultipleLocator(1))

            # Change minor ticks to show every 5. (20/4 = 5)
            # ax.xaxis.set_minor_locator(AutoMinorLocator(4))
            ax.yaxis.set_minor_locator(AutoMinorLocator(0.5))

            # Turn grid on for both major and minor ticks and style minor slightly
            # differently.
            ax.grid(which='major', color='#CCCCCC', linestyle='--')
            ax.grid(which='minor', color='#CCCCCC', linestyle=':')

            plt.show()

        print(len(self.recordTime))
        print(max([item[1] for item in self.actualJointTorqueRecord]))





